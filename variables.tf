variable "project_name" {
  type        = map
  default     = {
    master   = "proyecto-prueba-290222"
  }
}

variable "gcp_region" {
}

variable "gcp_zone" {
}

variable "cluster_name" {
}

variable "cluster_node_serviceaccount" {
  type        = map
  default     = {
    master  = "647035426660-compute@developer.gserviceaccount.com"
  }      
}

variable "cluster_autoescaling_min_cpu" {
  type        = map
  default     = {
    master  = 1
  }      
}

variable "cluster_autoescaling_max_cpu" {
  type        = map
  default     = {
    master  = 4
  }      
}

variable "cluster_autoescaling_min_memory" {
  type        = map
  default     = {
    master  = 16
  }      
}

variable "cluster_autoescaling_max_memory" {
  type        = map
  default     = {
    master  = 32
  }      
}

variable "node_count" {
  type        = map
  default     = {
    master  = 1
  }      
}

variable "node_autoscaling_min_node" {
  type        = map
  default     = {
    master  = 1
  }      
}

variable "node_autoscaling_max_node" {
  type        = map
  default     = {
    master  = 3
  }      
}

variable "node_disk_size" {
  type        = map
  default     = {
    master  = 30
  }      
}

variable "node_machine_type" {
  type        = map
  default     = {
    master  = "e2-highmem-4"
  }      
}

variable "env" {
  description = "env: master"
}
