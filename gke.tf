terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
    google-beta = {
      source = "hashicorp/google-beta"
    }
  }
}
provider "google" {
  version = "3.37.0"
  credentials = file("gcloud-service-key.json")
  project = lookup(var.project_name, var.env)
  region  = var.gcp_region
  zone    = var.gcp_zone
}

provider "google-beta" {
  version = "3.37.0"
  credentials = file("gcloud-service-key.json")
  project = lookup(var.project_name, var.env)
  region  = var.gcp_region
  zone    = var.gcp_zone
}

resource "google_container_cluster" "gke-prueba-api" {
  provider = google-beta
  name     = var.cluster_name
  location = var.gcp_zone
  enable_shielded_nodes = true
  remove_default_node_pool = true
  initial_node_count       = 1
  release_channel{
      channel = "REGULAR"
  }
  cluster_autoscaling{
    enabled = true
    auto_provisioning_defaults {
        oauth_scopes = [
        "https://www.googleapis.com/auth/logging.write",
        "https://www.googleapis.com/auth/monitoring",
        "https://www.googleapis.com/auth/devstorage.read_only",
        ]
        service_account = lookup(var.cluster_node_serviceaccount, var.env)
    }
    resource_limits {
         resource_type = "cpu"
         minimum = lookup(var.cluster_autoescaling_min_cpu, var.env)
         maximum = lookup(var.cluster_autoescaling_max_cpu, var.env)
      }
    resource_limits {
         resource_type = "memory"
         minimum = lookup(var.cluster_autoescaling_min_memory, var.env)
         maximum = lookup(var.cluster_autoescaling_max_memory, var.env)
      }
  }
  
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "gke-prueba-api-nodes" {
  name       = "principal-pool"
  location   = var.gcp_zone

  cluster    = google_container_cluster.gke-prueba-api.name
  node_count = lookup(var.node_count, var.env)
  autoscaling {
      min_node_count = lookup(var.node_autoscaling_min_node, var.env)
      max_node_count = lookup(var.node_autoscaling_max_node, var.env)
  }
  management {
      auto_repair = true
      auto_upgrade = true
  }
  upgrade_settings {
      max_surge = 1
      max_unavailable = 0
  }
  node_config {
    image_type = "COS_CONTAINERD"
    service_account = lookup(var.cluster_node_serviceaccount, var.env)
    disk_size_gb = lookup(var.node_disk_size, var.env)
    disk_type = "pd-standard"
    preemptible  = true
    machine_type = lookup(var.node_machine_type, var.env)

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
